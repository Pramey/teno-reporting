
package com.tenoapp.reporting;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.utils.ReflectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.MongoClient;

@Slf4j
@Configuration
@AutoConfigureAfter(MongoAutoConfiguration.class)
public class CMSMorphiaConfig {

    @Autowired
    private MongoClient mongoClient;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ApplicationContext applicationContext;

    @Bean
    Morphia morphia() {
        return new Morphia();
    }

    @Bean
    public Datastore datastore(Morphia morphia) throws ClassNotFoundException, IOException {

        List<String> packageNamesFromApplication = getApplicationPackageName(applicationContext);

        Set<Class<?>> classes = packageNamesFromApplication.parallelStream()
                .flatMap(packageName -> getClasses(packageName).parallelStream()).collect(Collectors.toSet());

        classes.parallelStream().filter(clazz -> Objects.nonNull(clazz.getAnnotation(Entity.class)))
                .forEach(clazz -> morphia.map(clazz));
        Datastore dataStore = morphia.createDatastore(mongoClient, mongoTemplate.getDb().getName());
        dataStore.ensureIndexes();
        return dataStore;
    }

    /**
     * Return root package of spring boot application.
     *
     * @param applicationContext
     * @return list of packages
     */
    public static List<String> getApplicationPackageName(final ApplicationContext applicationContext) {

        Set<String> candidateClasses = new HashSet<>();
        candidateClasses
                .addAll(Arrays.asList(applicationContext.getBeanNamesForAnnotation(SpringBootApplication.class)));

        if (candidateClasses.isEmpty()) {
            throw new RuntimeException(
                    "Is mandatory for the starter have @SpringBootApplication, @EnableAutoConfiguration or @ComponentScan annotation");
        } else {
            return candidateClasses.parallelStream()
                    .map(candidateClazz -> applicationContext.getBean(candidateClazz).getClass().getPackage().getName())
                    .distinct().collect(Collectors.toList());
        }

    }

    /**
     * Return classes from a package name.
     *
     * @param packageName
     * @return list of class
     */
    public static Set<Class<?>> getClasses(final String packageName) {
        try {
            return ReflectionUtils.getClasses(packageName, true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
