
package com.tenoapp.reporting.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.tenoapp.reporting.cms.repo.CMSStats;
import com.tenoapp.reporting.cms.repo.CMSStatsRepo;
import com.tenoapp.reporting.cms.service.AssessmentService;
import com.tenoapp.reporting.cms.service.WorksheetService;
import com.tenoapp.reporting.subscriptions.service.SubscriptionsService;

@Slf4j
@Service
public class AsyncReportingService {
    @Value("${script.start.date}")
    public String sDate;

    @Value("${run.script}")
    public boolean runScript;

    @Autowired
    SubscriptionsService subscriptionService;

    @Autowired
    private AssessmentService assessmentService;

    @Autowired
    private WorksheetService worksheetService;

    @Autowired
    private CMSStatsRepo cmsRepo;

    /*
     * Fetch questions from qod log which are next_date <= today (only gk questions and MCQ/TOF type) Attempt to create
     * QoD from questions if quiz is created mark(inc use_count,set next_date:=today+15days) the questions in qod log
     */
    @Scheduled(cron = "${teno.scheduler:0 0 0 * * *}")
    // once every day
    // at 00:00 hrs
    public void scheduleReport() {
        Date startDate;
        Calendar calDate = Calendar.getInstance();
        Date endDate;
        try {
            calDate.set(Calendar.HOUR_OF_DAY, 0);
            calDate.set(Calendar.MINUTE, 0);
            calDate.set(Calendar.SECOND, 0);
            calDate.set(Calendar.MILLISECOND, 0);

            endDate = calDate.getTime();
            calDate.add(Calendar.DATE, -1);
            startDate = calDate.getTime();

            log.error("Stats for start date: " + startDate + " and end date: " + endDate);
            storeCMSStats(startDate, endDate);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public CMSStats storeCMSStats(Date startDate, Date endDate) {
        CMSStats primeSubStats = this.cmsRepo.findByCreatedDate(startDate);

        // if (primeSubStats != null)
        // return primeSubStats;

        primeSubStats = this.subscriptionService.getPrimeSubStats(startDate, endDate);
        primeSubStats.setCreatedDate(startDate);
        primeSubStats.setTotalAssessmentCount(this.assessmentService.getTotalAssessmentCount(startDate, endDate));
        primeSubStats.setDistinctUsersTookAssessment(
                this.assessmentService.getDistinctAssesmentUserCount(startDate, endDate));
        primeSubStats.setTotalWorksheetShared(this.worksheetService.getSharedWorksheetsCount(startDate, endDate));
        primeSubStats.setDistinctTeachersSharedWorksheet(
                this.worksheetService.getWorksheetSharedByCount(startDate, endDate));

        this.cmsRepo.save(primeSubStats);
        return primeSubStats;
    }

    public void restAssessmentDistinct(Date startDate, Date endDate) {
        System.out.println("startDate: " + startDate + " endDate: " + endDate);
        System.out.println(this.assessmentService.getDistinctAssesmentUserCount(startDate, endDate));
        System.out.println("----------------------");
    }

    public void fetchData() throws ParseException {
        Date startDate;
        Calendar calDate = Calendar.getInstance();
        Date endDate;
        while (true) {
            try {
                calDate.set(Calendar.HOUR_OF_DAY, 0);
                calDate.set(Calendar.MINUTE, 0);
                calDate.set(Calendar.SECOND, 0);
                calDate.set(Calendar.MILLISECOND, 0);

                if (runScript) {
                    startDate = new SimpleDateFormat("dd-MM-yyyy").parse(sDate);
                    calDate.setTime(startDate);
                    calDate.add(Calendar.DATE, +1);
                    endDate = calDate.getTime();
                } else {
                    startDate = calDate.getTime();
                    calDate.add(Calendar.DATE, +1);
                    endDate = calDate.getTime();
                }
                log.error("Stats for start date: " + startDate + " and end date: " + endDate);

                storeCMSStats(startDate, endDate);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            runScript = false;
            if (calDate.get(Calendar.DATE) == Calendar.getInstance().get(Calendar.DATE)) {
                break;
            }
        }
    }

    public void testAssessment() {
        Date startDate;
        Calendar calDate = Calendar.getInstance();
        Date endDate;
        while (true) {
            try {
                calDate.set(Calendar.HOUR_OF_DAY, 0);
                calDate.set(Calendar.MINUTE, 0);
                calDate.set(Calendar.SECOND, 0);
                calDate.set(Calendar.MILLISECOND, 0);

                if (runScript) {
                    startDate = new SimpleDateFormat("dd-MM-yyyy").parse(sDate);
                    calDate.setTime(startDate);
                    calDate.add(Calendar.DATE, +1);
                    endDate = calDate.getTime();
                } else {
                    startDate = calDate.getTime();
                    calDate.add(Calendar.DATE, +14);
                    endDate = calDate.getTime();
                }
                log.error("Stats for start date: " + startDate + " and end date: " + endDate);

                System.out.println(this.assessmentService.getDistinctAssesmentUserCount(startDate, endDate));
                runScript = false;
                if (calDate.get(Calendar.DATE) == Calendar.getInstance().get(Calendar.DATE)) {
                    break;
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}
