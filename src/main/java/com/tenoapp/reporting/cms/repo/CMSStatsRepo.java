
package com.tenoapp.reporting.cms.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CMSStatsRepo extends JpaRepository<CMSStats, Integer> {

    CMSStats findByCreatedDate(Date startDate);
}
