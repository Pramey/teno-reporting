
package com.tenoapp.reporting.cms.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WorksheetShareRepo extends JpaRepository<WorksheetShare, Integer> {

    List<WorksheetShare> findAllByUserId(Integer userId, Pageable page);

    List<WorksheetShare> findByWorksheetIdAndUserIdAndGroupId(String worksheetId, Integer userId, Integer groupId);

    List<WorksheetShare> findByWorksheetIdAndUserId(String worksheetId, Integer userId);

    @Query("select count(distinct userId) from WorksheetShare")
    int countDistinctUserId();

    long countByShareDateBetween(Date startDate, Date endDate);

    @Query("select count(distinct userId) from WorksheetShare where shareDate > ?1 and shareDate < ?2")
    long countByDistinctUserIdAndShareDateBetween(Date startDate, Date endDate);
}
