
package com.tenoapp.reporting.cms.repo;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Property;

@Data
public class QuestionAttempt {
    @NotNull
    @Property(value = "question_id")
    String questionId;

    @Property(value = "time_taken")
    Long timeTaken;

    @NotNull
    @Embedded(value = "attempt_answers")
    List<Answer> attemptAnswers;

    @Property(value = "assessment_state")
    AssessmentState assessmentState;

    @Property(value = "marks_obtained")
    Float marksObtained;
}
