package com.tenoapp.reporting.cms.repo;

public enum AssessmentState {

    UNKNOWN, INCORRECT, CORRECT;
}
