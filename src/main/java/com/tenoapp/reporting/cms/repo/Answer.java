package com.tenoapp.reporting.cms.repo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import org.mongodb.morphia.annotations.Property;

@Data
public class Answer // full text serach, index mongo
{
    @ApiModelProperty(position = 1)
    @Property(value = "answer_id")
    String answerId;

    @ApiModelProperty(position = 2)
    @Property(value = "render_seq_no")
    Integer renderSequenceNo;

    @ApiModelProperty(position = 3)
    @Property(value = "text")
    String text;

    @ApiModelProperty(position = 4)
    @Property(value = "rich_text")
    String richText;

    @ApiModelProperty(position = 5)
    @Property(value = "image_url")
    String imageURL;

    @ApiModelProperty(position = 6)
    @Property(value = "video_url")
    String videoURL;
}