
package com.tenoapp.reporting.cms.repo;

public enum WorksheetLevel {
    BEGINNER, INTERMIDIATE, ADVANCED;
}
