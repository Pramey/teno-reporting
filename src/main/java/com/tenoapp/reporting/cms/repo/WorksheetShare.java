
package com.tenoapp.reporting.cms.repo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "worksheet_share", catalog = "db_cms", schema = "")
@XmlRootElement
public class WorksheetShare implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "worksheet_id", length = 50)
    private String worksheetId;

    @Column(name = "worksheet_title", length = 250)
    private String worksheetTitle;

    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Basic(optional = false)
    @Column(name = "user_name", length = 255)
    private String userName;

    @Basic(optional = false)
    @Column(name = "group_id", nullable = false)
    private int groupId;

    @Basic(optional = false)
    @Column(name = "school_id", nullable = false)
    private int schoolId;

    @Basic(optional = false)
    @Column(name = "author_id", nullable = false)
    private String authorId;

    @Basic(optional = false)
    @Column(name = "teno_account_id", length = 50)
    private String tenoAccountId;

    @Basic(optional = false)
    @Column(name = "share_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date shareDate;

    @Column(name = "worksheet_level", length = 50)
    private String worksheetLevel;

    @Column(name = "worksheet_type", length = 50)
    private String worksheetType;

    @Basic(optional = false)
    @Column(name = "question_count", nullable = false)
    private int questionCount;

    @Basic(optional = false)
    @Column(name = "is_deleted", nullable = false)
    private boolean isDeleted;

    public WorksheetShare() {
    }

    public WorksheetShare(Integer id) {
        this.id = id;
    }

    public WorksheetShare(Integer id, String worksheetId, int userId, int groupId, int schoolId, String authorId,
            String tenoAccountId, Date shareDate, int questionCount, boolean isDeleted) {
        this.id = id;
        this.worksheetId = worksheetId;
        this.userId = userId;
        this.groupId = groupId;
        this.schoolId = schoolId;
        this.authorId = authorId;
        this.tenoAccountId = tenoAccountId;
        this.shareDate = shareDate;
        this.questionCount = questionCount;
        this.isDeleted = isDeleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWorksheetId() {
        return worksheetId;
    }

    public void setWorksheetId(String worksheetId) {
        this.worksheetId = worksheetId;
    }

    public String getWorksheetTitle() {
        return worksheetTitle;
    }

    public void setWorksheetTitle(String worksheetTitle) {
        this.worksheetTitle = worksheetTitle;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(int schoolId) {
        this.schoolId = schoolId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getTenoAccountId() {
        return tenoAccountId;
    }

    public void setTenoAccountId(String tenoAccountId) {
        this.tenoAccountId = tenoAccountId;
    }

    public Date getShareDate() {
        return shareDate;
    }

    public void setShareDate(Date shareDate) {
        this.shareDate = shareDate;
    }

    public String getWorksheetLevel() {
        return worksheetLevel;
    }

    public void setWorksheetLevel(String worksheetLevel) {
        this.worksheetLevel = worksheetLevel;
    }

    public String getWorksheetType() {
        return worksheetType;
    }

    public void setWorksheetType(String worksheetType) {
        this.worksheetType = worksheetType;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof WorksheetShare)) {
            return false;
        }
        WorksheetShare other = (WorksheetShare) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.tenoapp.prime.model.WorksheetShare[ id=" + id + " ]";
    }

}
