
package com.tenoapp.reporting.cms.repo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.client.model.geojson.Point;

@Data
@Entity(value = "assessment", noClassnameStored = true)
public class Assessment {

    @org.mongodb.morphia.annotations.Id
    String id;

    @Property(value = "attempt_id")
    String attemptId;

    @NotNull
    @Property(value = "worksheet_id")
    String worksheetId;

    @Property(value = "level")
    WorksheetLevel worksheetLevel;

    @Property(value = "board")
    String board;

    @Property(value = "standard")
    String standard;

    @Property(value = "subject")
    String subject;

    @Property(value = "topic")
    String topic;

    @Property(value = "sub_topic")
    String subTopic;

    @Property(value = "total_marks")
    Float totalMarks;

    @Property(value = "passing_marks")
    Float passingMarks;

    @Property(value = "question_count")
    Integer questionCount = 0;

    @Property(value = "attempt_count")
    Integer attemptCount = 0;

    @Property(value = "author_id")
    String authorId;

    @Property(value = "author_name")
    String authorName;

    @NotNull
    @Property(value = "school_id")
    Integer schoolId;

    @NotNull
    @Property(value = "submitter_id")
    Integer submitterId;

    @Property(value = "submitter_teno_id")
    String submittorTenoId;

    @JsonIgnore
    @Property(value = "submitter_location")
    Point geoLocation;

    @Property(value = "submitter_latitude")
    Double latitude;

    @Property(value = "submitter_longitude")
    Double longitude;

    @Property(value = "start_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    Date startTime;

    @Property(value = "end_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    Date endTime;

    @Property(value = "time_taken")
    Long timeTaken;

    @Property(value = "assessment_score")
    Float assessmentScore;

    @Property(value = "submitted_time")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    Date submittedTime;

    @Property(value = "assessed_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    Date assessedDate;

    @NotNull
    @Embedded(value = "question_attempt")
    List<QuestionAttempt> questionsAttempt;

    @Property(value = "rating")
    String rating;

    @Property(value = "comment")
    String comment;

    @Property(value = "share_by_user_id")
    Integer shareByUserId = -1;

    @Property(value = "share_by_user_name")
    String shareByUserName = null;

}
