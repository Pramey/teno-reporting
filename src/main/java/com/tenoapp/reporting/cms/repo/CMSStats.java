package com.tenoapp.reporting.cms.repo;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "cms_stats", catalog = "db_cms", schema = "")
public class CMSStats {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;

    @Column(name = "created_date", nullable = false)
    Date createdDate = null;

    @Column(name = "total_active_prime_subscription", nullable = false)
    int totalActivePrimeSubscription = 0;

    @Column(name = "total_expired_prime_subscription", nullable = false)
    int totalExpiredPrimeSubscription = 0;

    @Column(name = "total_success_transaction", nullable = false)
    int totalSuccessTransaction = 0;

    @Column(name = "total_failed_transaction", nullable = false)
    int totalFailedTransaction = 0;

    @Column(name = "total_expired_transaction", nullable = false)
    int totalExpiredTransaction = 0;

    @Column(name = "total_cancel_transaction", nullable = false)
    int totalCancelTransaction = 0;

    @Column(name = "total_worksheet_shared", nullable = false)
    long totalWorksheetShared = 0;

    @Column(name = "distinct_teachers_shared_worksheet", nullable = false)
    long distinctTeachersSharedWorksheet = 0;

    @Column(name = "total_assessment_count", nullable = false)
    long totalAssessmentCount = 0;

    @Column(name = "distinct_users_took_assessment", nullable = false)
    long distinctUsersTookAssessment = 0;
}