
package com.tenoapp.reporting.cms.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.extern.slf4j.Slf4j;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tenoapp.reporting.cms.repo.Assessment;

@Slf4j
@Service
public class AssessmentService {

    @Autowired
    private Datastore datastore;

    public long getTotalAssessmentCount(Date startDate, Date endDate) {
        Query<Assessment> qry = datastore.find(Assessment.class);
        qry.criteria("submittedTime").greaterThanOrEq(startDate)
                .add(qry.criteria("submittedTime").lessThanOrEq(endDate));
        long count = qry.count();

        return count;
    }

    public int getDistinctAssesmentUserCount(Date startDate, Date endDate) {

        Set<Integer> submitterSet = new HashSet<Integer>();
        Query<Assessment> qry = datastore.find(Assessment.class);
        qry.criteria("submittedTime").greaterThanOrEq(startDate)
                .add(qry.criteria("submittedTime").lessThanOrEq(endDate));
        List<Assessment> assessments = qry.asList();
        assessments.parallelStream().forEach(assessment -> {
            submitterSet.add(assessment.getSubmitterId());
        });

        return submitterSet.size();
    }

}
