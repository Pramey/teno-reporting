
package com.tenoapp.reporting.cms.service;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.mongodb.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tenoapp.reporting.cms.repo.WorksheetShareRepo;

@Slf4j
@Service
public class WorksheetService {
    @Autowired
    WorksheetShareRepo worksheetShareRepo;

    @Autowired
    private Datastore datastore;

    public long getsharedWorksheetsCount() {
        return this.worksheetShareRepo.count();
    }

    public long getWorksheetSharedByCount(Date startDate, Date endDate) {
        return this.worksheetShareRepo.countByDistinctUserIdAndShareDateBetween(startDate, endDate);
    }

    public long getSharedWorksheetsCount(Date startDate, Date endDate) {
        return this.worksheetShareRepo.countByShareDateBetween(startDate, endDate);
    }
}
