package com.tenoapp.reporting.subscriptions.repo;

public enum PrimePaymentChannel {

    PAYU, INAPP_IOS, INAPP_ANDROID;

}
