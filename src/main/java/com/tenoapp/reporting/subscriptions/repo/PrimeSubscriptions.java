package com.tenoapp.reporting.subscriptions.repo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;

@Data
@Entity
@Table(name = "prime_subscriptions", catalog = "db_prime", schema = "", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "user_id" }) })
public class PrimeSubscriptions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "lastest_event", nullable = false, length = 20)
    private PrimeEventType lastestEvent;

    @Basic(optional = false)
    @Column(name = "sub_id", nullable = false, length = 100)
    private String subId;

    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(nullable = false, length = 20)
    private PrimeSubStatus status;

    @Basic(optional = false)
    @Column(nullable = false)
    private int phone;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "user_role", nullable = false, length = 100)
    private PrimeUserRole userRole;

    @Basic(optional = false)
    @Column(name = "school_id", nullable = false)
    private int schoolId;

    @Basic(optional = false)
    @Column(name = "plan_code", nullable = false, length = 100)
    private String planCode;

    @Basic(optional = false)
    @Column(name = "plan_interval", nullable = false)
    private int planInterval;

    @Basic(optional = false)
    @Column(name = "plan_amount", nullable = false)
    private int planAmount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Basic(optional = false)
    @Column(name = "sub_start_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date subStartDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @Basic(optional = false)
    @Column(name = "sub_end_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date subEndDate;

    @Column(name = "latest_prime_tx_id", length = 100, nullable = true)
    private String latestPrimeTxId;

    @Column(name = "latest_renewal_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date latestRenewalDate;

    @Basic(optional = false)
    @Column(name = "renewal_count", nullable = false)
    private int renewalCount;

    @Basic(optional = false)
    @Column(name = "latest_billed_amount", nullable = false)
    private int latestBilledAmount;

    @Basic(optional = false)
    @Column(name = "total_billed_amount", nullable = false)
    private int totalBilledAmount;

    @Basic(optional = false)
    @Column(name = "create_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Basic(optional = false)
    @Column(name = "update_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Column(name = "prime_tx_intent_id", length = 100, nullable = true)
    private String primeTxIntentId;

    @Basic(optional = false)
    @Column(name = "prime_tx_intent_date", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date primeTxIntentDate;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "plan_interval_unit", nullable = false, length = 20)
    private PrimePlanIntervalUnit planIntervalUnit;

    @PreUpdate
    @PrePersist
    public void setUpdateDate() {
        this.setUpdateDate(new Date());
    }

}
