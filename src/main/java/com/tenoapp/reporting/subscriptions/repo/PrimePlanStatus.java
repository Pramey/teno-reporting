package com.tenoapp.reporting.subscriptions.repo;

public enum PrimePlanStatus {

    ACTIVE, INACTIVE;

}
