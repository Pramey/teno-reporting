package com.tenoapp.reporting.subscriptions.repo;

public enum PrimeUserRole {

    PARENT, STUDENT, TEACHER;
}
