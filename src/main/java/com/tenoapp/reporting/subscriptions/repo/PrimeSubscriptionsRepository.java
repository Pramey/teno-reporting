package com.tenoapp.reporting.subscriptions.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrimeSubscriptionsRepository extends JpaRepository<PrimeSubscriptions, Integer> {
    int countByStatus(PrimeSubStatus active);

    int countByStatusAndCreateDateBetween(PrimeSubStatus active, Date startDate, Date endDate);
}
