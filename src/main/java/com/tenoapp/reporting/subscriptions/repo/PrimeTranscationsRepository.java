package com.tenoapp.reporting.subscriptions.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PrimeTranscationsRepository extends JpaRepository<PrimeTranscations, Integer> {
    int countByTxStatus(PrimeTxStatus success);

    int countByTxStatusAndCreateDateBetween(PrimeTxStatus success, Date startDate, Date endDate);
}