package com.tenoapp.reporting.subscriptions.repo;

public enum PrimeTxStatus {

    INITIATED, SUCCESS, FAILED, CANCELLED, EXPIRED;

}