package com.tenoapp.reporting.subscriptions.repo;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class TranscationIntent {

    private String primeTxId;

    @NotNull(message = "Field Can Not Be Null")
    private PrimeEventType eventType;

    @NotNull(message = "Field Can Not Be Null")
    private int userId;

    @NotNull(message = "Field Can Not Be Null")
    private String planCode;

    @NotNull(message = "Field Can Not Be Null")
    private String deviceType;

    private String deviceModel;

    private String osVersion;

    private String appVersion;

    private PrimeTxStatus txStatus;

}
