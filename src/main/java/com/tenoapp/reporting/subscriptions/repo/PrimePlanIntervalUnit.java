package com.tenoapp.reporting.subscriptions.repo;

public enum PrimePlanIntervalUnit {

    DAYS, MINUTES;
}
