package com.tenoapp.reporting.subscriptions.repo;

public enum PrimeEventType {
    START, SUB, REN, UNSUB, EXPIRED, INTENT, INTENT_CLEAR, IOS_SUCCESS, IOS_FAILED, ANDROID_SUCCESS, ANDROID_FAILED, PAYU_SUCCESS, PAYU_FAILED;
}
