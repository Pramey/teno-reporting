package com.tenoapp.reporting.subscriptions.repo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "prime_transcations", catalog = "db_prime", schema = "", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "prime_tx_id" }) })
public class PrimeTranscations implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer id;

    @Basic(optional = false)
    @Column(name = "prime_tx_id", nullable = false, length = 100)
    private String primeTxId;

    @Basic(optional = false)
    @Column(name = "sub_id", nullable = false, length = 100)
    private String subId;

    @Basic(optional = false)
    @Column(name = "user_id", nullable = false)
    private int userId;

    @Basic(optional = false)
    @Column(name = "plan_code", nullable = false, length = 250)
    private String planCode;

    @Basic(optional = false)
    @Column(name = "plan_amount", nullable = false)
    private int planAmount;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "event_type", nullable = false, length = 20)
    private PrimeEventType eventType;

    @Basic(optional = false)
    @Column(name = "channel_tx_id", length = 250)
    private String channelTxId;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "tx_status", nullable = false, length = 100)
    private PrimeTxStatus txStatus;

    @Basic(optional = false)
    @Column(name = "billed_amount")
    private int billedAmount;

    @Basic(optional = false)
    @Column(name = "device_type", nullable = false, length = 20)
    private String deviceType;

    @Column(name = "device_model", length = 100)
    private String deviceModel;

    @Column(name = "os_version", length = 100)
    private String osVersion;

    @Column(name = "app_version", length = 100)
    private String appVersion;

    @Column(name = "payu_payment_mode", length = 100)
    private String payuPaymentMode;

    @Column(name = "ios_tx_data", length = 2048)
    private String iosTxData;

    @Column(name = "android_tx_data", length = 2048)
    private String androidTxData;

    @Column(name = "tx_email_id", length = 250)
    private String txEmailId;

    @Column(name = "tx_ip", length = 50)
    private String txIp;

    @Basic(optional = false)
    @Column(name = "create_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Basic(optional = false)
    @Column(name = "update_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "payment_channel", nullable = false, length = 20)
    private PrimePaymentChannel paymentChannel;

    @PreUpdate
    @PrePersist
    public void setUpdateDate() {
        this.setUpdateDate(new Date());
    }

}
