package com.tenoapp.reporting.subscriptions.repo;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;

@Data
public class Subscription {

    private String subId;

    private PrimeSubStatus status;

    @NotNull(message = "Field Can Not Be Null")
    private int userId;

    @NotNull(message = "Field Can Not Be Null")
    private String planCode;

    private Integer planAmount;

    @NotNull(message = "Field Can Not Be Null")
    private PrimeUserRole userRole;

    @NotNull(message = "Field Can Not Be Null")
    private int schoolId;

    // "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Asia/Kolkata")
    private Date subStartDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Asia/Kolkata")
    private Date subEndDate;

    private String primeTxIntentId;

    private Date primeTxIntentDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", timezone = "Asia/Kolkata")
    private Date currentDate;

    private Boolean renewEnabled;

    private String primeDigest;

    private PrimePlanIntervalUnit planIntervalUnit;

}
