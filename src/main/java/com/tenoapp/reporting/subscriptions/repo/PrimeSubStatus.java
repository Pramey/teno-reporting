package com.tenoapp.reporting.subscriptions.repo;

public enum PrimeSubStatus {

    SUB_PAYMENT_PENDING, ACTIVE, REN_PAYMENT_PENDING, EXPIRED;

}
