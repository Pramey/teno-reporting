package com.tenoapp.reporting.subscriptions.service;

import java.util.Date;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tenoapp.reporting.cms.repo.CMSStats;
import com.tenoapp.reporting.subscriptions.repo.PrimeSubStatus;
import com.tenoapp.reporting.subscriptions.repo.PrimeSubscriptionsRepository;
import com.tenoapp.reporting.subscriptions.repo.PrimeTranscationsRepository;
import com.tenoapp.reporting.subscriptions.repo.PrimeTxStatus;

@Slf4j
@Service
public class SubscriptionsService {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PrimeSubscriptionsRepository primeSubscriptionsRepository;

    @Autowired
    private PrimeTranscationsRepository primeTranscationsRepository;

    public CMSStats getPrimeSubStats() {
        log.info("~ SubscriptionsService.getPrimeSubStats ~");

        CMSStats stats = new CMSStats();

        stats.setTotalSuccessTransaction(this.primeTranscationsRepository.countByTxStatus(PrimeTxStatus.SUCCESS));
        stats.setTotalFailedTransaction(this.primeTranscationsRepository.countByTxStatus(PrimeTxStatus.FAILED));
        stats.setTotalExpiredTransaction(this.primeTranscationsRepository.countByTxStatus(PrimeTxStatus.EXPIRED));
        stats.setTotalCancelTransaction(this.primeTranscationsRepository.countByTxStatus(PrimeTxStatus.CANCELLED));
        stats.setTotalActivePrimeSubscription(this.primeSubscriptionsRepository.countByStatus(PrimeSubStatus.ACTIVE));
        stats.setTotalExpiredPrimeSubscription(this.primeSubscriptionsRepository.countByStatus(PrimeSubStatus.EXPIRED));

        log.info("~ SubscriptionsService.getPrimeSubStats stats: {}~", stats);

        return stats;
    }

    public CMSStats getPrimeSubStats(Date startDate, Date endDate) {
        log.info("~ SubscriptionsService.getPrimeSubStats ~");

        CMSStats stats = new CMSStats();

        stats.setTotalSuccessTransaction(this.primeTranscationsRepository
                .countByTxStatusAndCreateDateBetween(PrimeTxStatus.SUCCESS, startDate, endDate));
        stats.setTotalFailedTransaction(this.primeTranscationsRepository
                .countByTxStatusAndCreateDateBetween(PrimeTxStatus.FAILED, startDate, endDate));
        stats.setTotalExpiredTransaction(this.primeTranscationsRepository
                .countByTxStatusAndCreateDateBetween(PrimeTxStatus.EXPIRED, startDate, endDate));
        stats.setTotalCancelTransaction(this.primeTranscationsRepository
                .countByTxStatusAndCreateDateBetween(PrimeTxStatus.CANCELLED, startDate, endDate));
        stats.setTotalActivePrimeSubscription(this.primeSubscriptionsRepository
                .countByStatusAndCreateDateBetween(PrimeSubStatus.ACTIVE, startDate, endDate));
        stats.setTotalExpiredPrimeSubscription(this.primeSubscriptionsRepository
                .countByStatusAndCreateDateBetween(PrimeSubStatus.EXPIRED, startDate, endDate));

        log.info("~ SubscriptionsService.getPrimeSubStats stats: {}~", stats);

        return stats;
    }
}
