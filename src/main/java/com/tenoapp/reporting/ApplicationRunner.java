
package com.tenoapp.reporting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.tenoapp.reporting.service.AsyncReportingService;

//@SpringBootApplication
@EnableAutoConfiguration
@EnableMongoRepositories
@ComponentScan(basePackages = "com.tenoapp.reporting")
public class ApplicationRunner implements CommandLineRunner {
    @Autowired
    AsyncReportingService asyncReportingService;

    public static void main(String[] args) {
        SpringApplication.run(ApplicationRunner.class, args);
    }

    public void run(String... args) throws Exception {
        System.out.println("started execution");
        asyncReportingService.fetchData();
        // asyncReportingService.testAssessment();
    }
}